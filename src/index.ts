import express from 'express'
import morgan from 'morgan'
const path = require('path')

// express
// cors
// morgan
// helmet
// monk

const publicDir = path.resolve(__dirname, '../public')

var app = express()

var port:number = 3000

app.use(morgan('tiny'))

app.use(express.static('public'))

app.get("/", function(req, res){
	res.sendFile(`home.html`, {root: publicDir})
})

app.get("/projects", function(req, res){
	res.sendFile(`projects.html`, {root: publicDir})
})

app.get("/apps", function(req, res){
	res.sendFile(`apps.html`, {root: publicDir})
})

app.listen(port, function(){
	console.log(`Listening on port ${port}`)
})
